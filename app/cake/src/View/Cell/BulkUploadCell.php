<?php
namespace App\View\Cell;

use Cake\View\Cell;

class BulkUploadCell extends Cell
{
    public function display($table, $error_list, $header, $entities)
    {
        $this->set(compact('table', 'error_list', 'header', 'entities'));
    }

    public function confirmation($error_list, $header, $entities)
    {
        $this->set(compact('error_list', 'header', 'entities'));
    }
}
