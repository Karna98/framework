<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ArtifactsPublication Entity
 *
 * @property int $id
 * @property int $artifact_id
 * @property int $publication_id
 * @property string|null $exact_reference
 * @property string|null $publication_type
 *
 * @property \App\Model\Entity\Artifact $artifact
 * @property \App\Model\Entity\Publication $publication
 */
class ArtifactsPublication extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'artifact_id' => true,
        'publication_id' => true,
        'exact_reference' => true,
        'publication_type' => true,
        'publication_comments' => true,
        'artifact' => true,
        'publication' => true
    ];

    protected function _setArtifactId($value)
    {
        // Format for P value input
        return ltrim($value, "P0");
    }
}
