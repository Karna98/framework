<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Genre Entity
 *
 * @property int $id
 * @property string $genre
 * @property int|null $parent_id
 * @property string|null $genre_comments
 *
 * @property \App\Model\Entity\Genre $parent_genre
 * @property \App\Model\Entity\Genre[] $child_genres
 * @property \App\Model\Entity\Artifact[] $artifacts
 */
class Genre extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'genre' => true,
        'parent_id' => true,
        'genre_comments' => true,
        'parent_genre' => true,
        'child_genres' => true,
        'artifacts' => true
    ];
}
