<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Author Entity
 *
 * @property int $id
 * @property string $author
 *
 * @property \App\Model\Entity\CdlNote[] $cdl_notes
 * @property \App\Model\Entity\Credit[] $credits
 * @property \App\Model\Entity\User[] $users
 * @property \App\Model\Entity\Publication[] $publications
 */
class Author extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

    protected function _setEmail($value)
    {
        if ($value == '') {
            $value = null;
        }

        return $value;
    }
}
