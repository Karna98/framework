<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Users Model
 *
 * @property \App\Model\Table\AuthorsTable|\Cake\ORM\Association\BelongsTo $Authors
 *
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UsersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('users');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Authors', [
            'foreignKey' => 'author_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmpty('id', 'create')
            ->add('id', 'unique', [
                'rule' => 'validateUnique',
                'provider' => 'table'
            ]);

        $validator
            ->scalar('username')
            ->maxLength('username', 255)
            ->allowEmpty('username', false, 'Username field cannot be left empty.')
            ->add('username', 'unique', [
                'rule' => 'validateUnique',
                'provider' => 'table',
                'message' => 'This username is already associated with an account.'
            ]);
        
        $validator
            ->email('email')
            ->allowEmpty('email', false, 'Email field cannot be left empty.')
            ->add('email', 'unique', [
                'rule' => 'validateUnique',
                'provider' => 'table',
                'message' => 'This email address is already associated with an account.'
            ]);

        $validator
            ->nonNegativeInteger('author_id')
            ->allowEmpty('author_id')
            ->add('author_id', 'unique', [
                'rule' => 'validateUnique',
                'provider' => 'table'
            ]);

        $validator
            ->dateTime('last_login_at')
            ->allowEmpty('last_login_at');

        $validator
            ->boolean('active');
        
        $validator
            ->scalar('password')
            ->lengthBetween('password', [8, 128], 'Password must be between 8 and 128 characters in length.')
            ->allowEmpty('password', false, 'Password cannot be empty.');

        $validator
            ->sameAs('password_confirm', 'password', 'Passwords do not match.')
            ->allowEmpty('password_confirm', false, 'Confirm Password cannot be empty.');
        
        $validator
            ->scalar('2fa_key')
            ->maxLength('2fa_key', 255)
            ->allowEmpty('2fa_key');

        $validator
            ->boolean('2fa_status')
            ->allowEmpty('2fa_status');

        $validator
            ->dateTime('created_at')
            ->allowEmpty('created_at', 'create');

        $validator
            ->dateTime('modified_at')
            ->allowEmpty('modified_at');

        $validator
            ->scalar('token_pass')
            ->allowEmpty('token_pass');

        $validator
            ->dateTime('generated_at')
            ->allowEmpty('generated_at');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['username']));
        $rules->add($rules->isUnique(['email']));
        $rules->add($rules->isUnique(['id']));
        $rules->add($rules->existsIn(['author_id'], 'Authors'));

        return $rules;
    }
}
