<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Postings Controller
 *
 * @property \App\Model\Table\PostingsTable $Postings
 *
 * @method \App\Model\Entity\Posting[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PostingsController extends AppController
{

    /**
     * News method
     *
     * @return \Cake\Http\Response|void
     */
    public function news()
    {
        $this->paginate = [
            'contain' => ['PostingTypes', 'Artifacts']
        ];
        $postings = $this->paginate($this->Postings);

        $this->set(compact('postings'));
    }

    /**
     * Highlights method
     *
     * @return \Cake\Http\Response|void
     */
    public function highlights()
    {
        $this->paginate = [
            'contain' => ['PostingTypes', 'Artifacts']
        ];
        $postings = $this->paginate($this->Postings);

        $this->set(compact('postings'));
    }

    /**
     * View method
     *
     * @param string|null $id Posting id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $posting = $this->Postings->get($id, [
            'contain' => ['PostingTypes', 'Artifacts']
        ]);

        $this->set('posting', $posting);
    }
}
