<?php

namespace App\Controller\Admin;

use App\Controller\AppController;

class EditorsPublicationsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'order' => [
                'editor_id' => 'ASC',
                'publication_id' => 'ASC'
            ],
            'contain' => ['Publications', 'Authors']
        ];
        $authorsPublications = $this->paginate($this->EditorsPublications);

        $this->set(compact('authorsPublications'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add($flag = 0, $id = null)
    {
        $authors_list = $this->EditorsPublications->Authors->find('all');
        $authors_mapping = [];
        foreach ($authors_list as $row) {
            $authors_mapping[$row['author']] = $row['id'];
        }
        $authors_names = array_keys($authors_mapping);
        $authorsPublication = $this->EditorsPublications->newEntity();
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            if (in_array($data['editor_id'], $authors_names)) {
                $data['editor_id'] = $authors_mapping[$data['editor_id']];
            } else {
                $data['editor_id'] = 0;
            }
            $authorsPublication = $this->EditorsPublications->patchEntity($authorsPublication, $data);
            if ($this->EditorsPublications->save($authorsPublication)) {
                $this->Flash->success(__('New link has been saved.'));
                return $this->redirect(['action' => 'add']);
            } else {
                $this->Flash->error(__('The link could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('authorsPublication', 'authors_names'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Authors Publication id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null, $flag = 0, $parent_id = null)
    {
        $authorsPublication = $this->EditorsPublications->get($id, [
                        'contain' => ['Publications', 'Authors']
                    ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $authorsPublication = $this->EditorsPublications->patchEntity($authorsPublication, $this->request->getData());
            if ($this->EditorsPublications->save($authorsPublication)) {
                $this->Flash->success(__('Changes has been saved.'));

                if ($flag == 0) {
                    return $this->redirect(['action' => 'index']);
                } else {
                    return $this->redirect(['controller' => 'AuthorsPublications', 'action' => 'add', $flag, $parent_id]);
                }
            }
            $this->Flash->error(__('Changes could not be saved. Please, try again.'));
        }
        $authorsPublications = $this->EditorsPublications->find('all', ['contain' => ['Authors', 'Publications']])->where(['publication_id' => $authorsPublication->publication_id])->all();
        $this->set(compact('authorsPublication', 'flag', 'parent_id', 'authorsPublications'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Authors Publication id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null, $flag = 0, $parent_id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $authorsPublication = $this->EditorsPublications->get($id);
        if ($this->EditorsPublications->delete($authorsPublication)) {
            $this->Flash->success(__('The link has been deleted.'));
        } else {
            $this->Flash->error(__('The link could not be deleted. Please, try again.'));
        }

        if ($flag == 0) {
            return $this->redirect(['action' => 'index']);
        } else {
            return $this->redirect(['controller' => 'AuthorsPublications', 'action' => 'add', $flag, $parent_id]);
        }
    }
}
