<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;

class BulkUploadComponent extends Component
{
    public function initialize(array $config)
    {
        $this->table = $config['table'];
        $this->Table = TableRegistry::get($this->table);

        $this->header_mapping = [];

        if ($this->table == 'ArtifactsPublications') {
            $this->header_mapping = [
                'bibtexkey' => 'publication_id'
            ];
        }
        $this->header_mapping_reverse = array_flip($this->header_mapping);
    }
    
    /**
     * Get information about each field of the database table.
     *
     * @return string
     */
    public function getTableColumns()
    {
        // Convert camel case to snake case
        $table_name = strtolower(preg_replace('/(?<!^)[A-Z]/', '_$0', $this->table));
        $db = ConnectionManager::get('default');

        // Create a schema collection
        $collection = $db->schemaCollection();

        // Get a single table (instance of Schema\TableSchema)
        $tableSchema = $collection->describe($table_name);
        
        // Get columns list from table
        $columns = $tableSchema->columns();
        
        // Remove 'id' from the list as it is provided by the database
        unset($columns[0]);

        return $columns;
    }
    
    /**
     * Load data from the CSV file and check for file format errors.
     *
     * @param array $data Meta data for the uploaded file.
     *
     * @return string[] $file_error List of error messages for any type of file format error.
     * @return string[] $all_rows List of the data read from the file.
     * @return string[] $raw_data List of each line of the file.
     * @return string[] $header List of headers for the file.
     */
    public function load($data)
    {
        $file_error = array();
        $all_rows = array();
        $raw_data = array();
        $header = array();
        if ($data['type'] == 'text/csv') {
            // Import data and format errors
                    
            $file = $data['tmp_name'];
            $file = fopen($file, "r");
            $header = fgetcsv($file);
            
            // Convert headers to required format for saving in database
            $header_mapped = array();
            foreach ($header as $key => $value) {
                $header_mapped[$key] = (isset($this->header_mapping[$value]) ? $this->header_mapping[$value]:$value);
            }
            
            // Check empty csv file
            if (!isset($header_mapped[0])) {
                array_push($file_error, 'The CSV file is empty');
            } else {
                $table_columns = $this->getTableColumns();
                
                // Check if the headers are correct
                $extra = array();
                foreach ($header_mapped as $field) {
                    if (!in_array($field, $table_columns)) {
                        array_push($extra, ($field != '') ? $field:'<empty_field>');
                    }
                }

                if ($extra != []) {
                    array_push($file_error, 'The following fields are not in the table and hence need to be removed from the file: ['.implode(', ', $extra).']. Please check that the file headers have been set properly');
                }

                // Check if all necessary (not null) fields are present
                $missing = array();
                foreach ($table_columns as $field) {
                    if (!in_array($field, $header_mapped)) {
                        array_push($missing, (isset($this->header_mapping_reverse[$field]) ? $this->header_mapping_reverse[$field] : $field));
                    }
                }

                if ($missing != []) {
                    array_push($file_error, 'The following necessary fields need to be present in the file: ['.implode(', ', $missing).']. Please check that the file headers have been set properly');
                }

                // If there is no error in file format, we get the list of values
                if ($file_error == []) {
                    while ($row = fgetcsv($file)) {
                        $all_rows[] = array_combine($header_mapped, $row);
                        array_push($raw_data, implode(',', $row));
                    }
                }
            }
            fclose($file);
        } else {
            array_push($file_error, ($data['type'] == '') ? 'No file uploaded' : 'The uploaded file must be in CSV format');
        }

        return array($file_error, $all_rows, $raw_data, $header);
    }

    /**
     * Validate the provided data.
     *
     * @param string[] $all_rows List of the data read from the file.
     * @param string[] $raw_data List of each line of the file.
     *
     * @return array $error_list List of all the objects containing errors.
     * @return array $entities List of all the entity objects.
     */
    public function validate($all_rows, $raw_data)
    {
        // Data validation errors
        $error_flag = 0;
        $entities = $this->Table->newEntities($all_rows);
        $entities = $this->Table->patchEntities($entities, $all_rows);

        // For getting the rules checker output (no better way found for only checking rules as it is coupled to save operations).
        foreach ($entities as $key => $entity) {
            if ($this->Table->save($entity)) {
                $this->Table->delete($entity);
                $entity = $this->Table->newEntity($all_rows[$key]);
            }
        }

        // Creating a list of entities with and without errors.
        $entities_error = array();
        foreach ($entities as $key => $row) {
            if (!empty($row->errors())) {
                $row->line_number = $key + 2;
                $row->raw_data = $raw_data[$key];
                array_push($entities_error, $row);
            }
        }

        // Creating a list of data errors.
        $error_list = array();
        foreach ($entities_error as $entity) {
            $id = null;
            $error_msgs = array();
            $break_flag = 0;
            foreach ($entity->errors() as $field => $error) {
                foreach ($error as $key => $value) {
                    if ($key == '_isUnique') {
                        array_push($error_msgs, $value);
                        $break_flag = 1;
                        $id = $this->getId($entity, $this->getTableColumns());
                        break;
                    }
                    array_push($error_msgs, (isset($this->header_mapping_reverse[$field]) ? $this->header_mapping_reverse[$field] : $field).': '.$value);
                }
                if ($break_flag == 1) {
                    break;
                }
            }
            array_push($error_list, [$entity->line_number, $entity->raw_data, $error_msgs, $id]);
        }

        // Create new entities for save operations
        $entities = $this->Table->newEntities($all_rows);
        $entities = $this->Table->patchEntities($entities, $all_rows);
        
        return array(($error_list != []) ? $error_list: null, $entities);
    }

    /**
     * Get ID of the entity that already exists.
     */
    public function getId($entity, $fields)
    {
        $conditions = array();
        foreach ($fields as $field) {
            $conditions[$field] = $entity[$field];
        }

        return $this->Table->find('all', ['conditions' => $conditions])->first()->id;
    }

    /**
     * Saving all the entries without errors.
     *
     * @param array $entities List of all the entity objects.
     */
    public function save($entities)
    {
        foreach ($entities as $entity) {
            $this->Table->save($entity);
        }
    }

    /**
     * Download CSV file containing entries that couldn't be saved due to errors.
     *
     * @param array $error_list_serialize Serialized list of all the objects containing errors.
     * @param array $header_mapped_serialize Serialized list of headers for the file.
     *
     * @return string[] $data Formatted array of data for saving to a CSV file.
     * @return array $_serialize Serialize information for the generation of CSV file.
     * @return array $_header Headers for the generation of CSV file.
     */
    public function export($error_list_serialize, $header_serialize)
    {
        $error_list = unserialize($error_list_serialize);
        $_header = unserialize($header_serialize);
        $this->response->download('errors.csv');
        $data = array();
        foreach ($error_list as $row) {
            if ($row[2][0] != 'This link already exists') {
                array_push($data, explode(',', $row[1]));
            }
        }
        $_serialize = 'data';

        return array($data, $_serialize, $_header);
    }
}
