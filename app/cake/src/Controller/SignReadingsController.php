<?php
declare(strict_types=1);

namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * SignReadings Controller
 *
 * @property \App\Model\Table\SignReadingsTable $SignReadings
 *
 * @method \App\Model\Entity\SignReading[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class SignReadingsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Periods', 'Proveniences', 'Languages', 'SignReadingsComments'],
        ];
        $signReadings = $this->paginate($this->SignReadings);

        $this->set(compact('signReadings'));
    }
    /**
     * View method
     *
     * @param string|null $id Sign Reading id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */

    public function view($id=null, $id2=null)
    {
        if ($id=='preferred') {
            $this->paginate = [
                'contain' => ['Periods', 'Proveniences', 'Languages', 'SignReadingsComments'],
            ];
            $signReadings = $this->paginate($this->SignReadings);

            $this->set(compact('signReadings'));
            $this->render('/SignReadings/preferred');
        } elseif ($id=='view_preferred') {
            $signReading = $this->SignReadings->get($id2, [
                'contain' => ['Periods', 'Proveniences', 'Languages', 'SignReadingsComments'],
            ]);

            $this->set('signReading', $signReading);
            $this->render('/SignReadings/view_preferred');
        } else {
            $signReading = $this->SignReadings->get($id, [
                'contain' => ['Periods', 'Proveniences', 'Languages', 'SignReadingsComments'],
            ]);

            $this->set('signReading', $signReading);
        }
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $signReading = $this->SignReadings->newEmptyEntity();
        if ($this->request->is('post')) {
            $signReading = $this->SignReadings->patchEntity($signReading, $this->request->getData());
            if ($this->SignReadings->save($signReading)) {
                $this->Flash->success(__('The sign reading has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The sign reading could not be saved. Please, try again.'));
        }
        $periods = $this->SignReadings->Periods->find('list', ['limit' => 200]);
        $proveniences = $this->SignReadings->Proveniences->find('list', ['limit' => 200]);
        $languages = $this->SignReadings->Languages->find('list', ['limit' => 200]);
        $this->set(compact('signReading', 'periods', 'proveniences', 'languages'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Sign Reading id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $signReading = $this->SignReadings->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $signReading = $this->SignReadings->patchEntity($signReading, $this->request->getData());
            if ($this->SignReadings->save($signReading)) {
                $this->Flash->success(__('The sign reading has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The sign reading could not be saved. Please, try again.'));
        }
        $periods = $this->SignReadings->Periods->find('list', ['limit' => 200]);
        $proveniences = $this->SignReadings->Proveniences->find('list', ['limit' => 200]);
        $languages = $this->SignReadings->Languages->find('list', ['limit' => 200]);
        $this->set(compact('signReading', 'periods', 'proveniences', 'languages'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Sign Reading id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $signReading = $this->SignReadings->get($id);
        if ($this->SignReadings->delete($signReading)) {
            $this->Flash->success(__('The sign reading has been deleted.'));
        } else {
            $this->Flash->error(__('The sign reading could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
