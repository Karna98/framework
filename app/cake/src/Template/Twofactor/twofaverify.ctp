<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User[]|\Cake\Collection\CollectionInterface $users
 */
?>

<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <div class="capital-heading text-center">Enter 2FA Code</div>
        <?= $this->Flash->render() ?>
        
        <?= $this->Form->create() ?>
        
            <?= $this->Form->control('code', ['class' => 'form-control centre', 'autocomplete' => 'off']) ?>
            <div class="text-center">    
            <?= $this->Form->submit('Submit', ['class' => 'form-control btn btn-primary col-md-2']); ?>
            </div>

        <?= $this->Form->end() ?>
    </div>

</div>

<script language="JavaScript" type="text/javascript">
    $(document).ready(function () {
                setTimeout(function(){
                  location.reload(true);
                }, 150000);       
            });    
</script>