<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactsCredit $artifactsCredit
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Artifacts Credit'), ['action' => 'edit', $artifactsCredit->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Artifacts Credit'), ['action' => 'delete', $artifactsCredit->id], ['confirm' => __('Are you sure you want to delete # {0}?', $artifactsCredit->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Artifacts Credits'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Artifacts Credit'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Artifacts'), ['controller' => 'Artifacts', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Artifact'), ['controller' => 'Artifacts', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Credits'), ['controller' => 'Credits', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Credit'), ['controller' => 'Credits', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="artifactsCredits view large-9 medium-8 columns content">
    <h3><?= h($artifactsCredit->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Artifact') ?></th>
            <td><?= $artifactsCredit->has('artifact') ? $this->Html->link($artifactsCredit->artifact->id, ['controller' => 'Artifacts', 'action' => 'view', $artifactsCredit->artifact->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Credit') ?></th>
            <td><?= $artifactsCredit->has('credit') ? $this->Html->link($artifactsCredit->credit->id, ['controller' => 'Credits', 'action' => 'view', $artifactsCredit->credit->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Credit Type') ?></th>
            <td><?= h($artifactsCredit->credit_type) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($artifactsCredit->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Date') ?></th>
            <td><?= h($artifactsCredit->date) ?></td>
        </tr>
    </table>
</div>
