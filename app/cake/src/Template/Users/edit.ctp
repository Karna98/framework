<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <div class="capital-heading"><?= __('Edit User') ?></div>

        <?= $this->Form->create("$user",['type'=>'post']) ?>
            <table class="table-bootstrap">
                <tbody>
                    <tr>
                        <th scope="row"><?= __('Username') ?></th>
                        <td><?= h($user->username) ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><?= __('Email') ?></th>
                        <td><?= h($user->email) ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><?= __('Created') ?></th>
                        <td><?= h($user->created_at) ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><?= __('New Password') ?></th>
                        <td><?= $this->Form->control('password',['class' => 'col-12 input-background-child-md', 'label' => false]) ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><?= __('Confirm New Password') ?></th>
                        <td><?= $this->Form->control('password_confirm',['class' => 'col-12 input-background-child-md', 'label' => false, 'type' => 'password']) ?></td>
                    </tr>
                </tbody>
            </table>
            <?= $this->Form->submit() ?>
        <?= $this->Form->end() ?>
    </div>

    <div class="col-lg boxed">
        <div class="capital-heading"><?= __('Related Actions') ?></div>
        <?= $this->Html->link(__('View your profile'), ['action' => 'view', $user->username], ['class' => 'btn-action']) ?>
        <?= $this->Flash->render() ?>
    </div>

</div>
