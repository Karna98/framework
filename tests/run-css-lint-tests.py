import unittest
import os

# 
# Framework uses main.css which is built using the main.scss
# CSSLint is used for minor custom CSS
# cake.css, home.css, dashboard.css, cover.css & signin.css.
#
class RunCssLintTests(unittest.TestCase):
    # Command prerequisite for CSSLint.
    css_lint_flags = [
        'csslint --quiet',
        '--errors=order-alphabetical,zero-units,selector-newline,empty-rules,duplicate-properties,errors,known-properties',
    ]
    relative_fpaths = ['./webroot/assets/css/', './app/cake/webroot/css/']
    command_exe = ['cd ./app/cake/webroot/css/; ', 'cd webroot/assets/css/; ']
    
    def test_css_lint_home_webroot(self):
        self.maxDiff = None
        # We have CSS files in the directory ./webroot/assets/css in which,
        # few CSS files are generated from SCSS they are ['base.css','filter.css','main.css']
        # and the custom written onces are
        # ['cake.css', 'home.css', 'dashboard.css', 'home.css', 'cover.css', 'signin.css']
        # this function lints the custom written CSS files.
        command_list_webroot_css = os.popen(self.command_exe[1]+' ls | grep ".css";')
        list_webroot_assets_css_files = command_list_webroot_css.read().split('\n')
        command_list_webroot_css.close()
        
        css_files_generated_by_scss = ['base.css','filter.css','main.css']
        
        custom_css_files_lint = [css_file for css_file in list_webroot_assets_css_files if css_file not in css_files_generated_by_scss]
        custom_css_files_lint.pop()

        print ("Running CSSlint tests for "+self.relative_fpaths[0]+"...")
        for css_file in custom_css_files_lint:
            run_csslint_cmd = os.popen(self.css_lint_flags[0]+' '+self.relative_fpaths[0]+css_file+' '+self.css_lint_flags[1])
            run_csslint_cmd_result = run_csslint_cmd.read()
            # For better linting display.
            if(run_csslint_cmd_result==''):
                result = 'passed!'
            else:
                result = 'failed!'
            print ("Linting "+self.relative_fpaths[0]+css_file+"......... "+result)
            self.assertEqual(run_csslint_cmd_result, '')
            run_csslint_cmd.close()
            
    def test_css_lint_cake_webroot(self):
        # We have CSS files in the cake webroot directory.
        # This test css files inside cake directory.
        command_list_webroot_css = os.popen(self.command_exe[0]+' ls | grep ".css";')
        list_webroot_assets_css_files = command_list_webroot_css.read().split('\n')
        command_list_webroot_css.close()
        
        css_files_generated_by_scss = ['base.css']
        
        custom_css_files_lint = [css_file for css_file in list_webroot_assets_css_files if css_file not in css_files_generated_by_scss]
        custom_css_files_lint.pop()

        print ("Running combined CSSlint tests for "+self.relative_fpaths[1]+"...")
        for css_file in custom_css_files_lint:
            run_csslint_cmd = os.popen(self.css_lint_flags[0]+' '+self.relative_fpaths[1]+css_file+' '+self.css_lint_flags[1])
            run_csslint_cmd_result = run_csslint_cmd.read()
            
            # For better linting display.
            if(run_csslint_cmd_result==''):
                result = 'passed!'
            else:
                result = 'failed!'

            print ("Linting "+self.relative_fpaths[1]+css_file+"......... "+result)
            self.assertEqual(run_csslint_cmd_result, '')
            run_csslint_cmd.close()
            
if __name__ == '__main__':
    unittest.main()
